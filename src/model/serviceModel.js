const mongoose = require('mongoose')

var serviceSchema = new mongoose.Schema({
    title: {
        type: String,
        trim: true,
        required: true
    },
    prepared_for:{
        type: String,
        trim: true,
        required: true,
    },
    date: {
       type: String,
       required: true
     },
    registered_feature_by_client:{
        type: String,
        trim: true,
        required: true
        },
    programming_language: {
          type: String,
          required: true
         },
    frameWork: {
         type: Object,
         frontEnd: {
             type: String
         },
         backEnd:{
            type: String,
         }
        },
    platform_and_database: {
        type: String,
        required: true
    },    
    discovery_MVP_planning: {
            type: Object,
            task:{
                type: String,
                trim: true
            },
            status: {
                type: String,
                trim: true
            },
            who:{
                type: String,
                trim: true
            },
            comment: {
                type: String,
                trim: true
            }
      },
    MVP_analysis_design_plan: {
            type: Object,
            task:{
                type: String,
                trim: true
            },
            status: {
                type: String,
                trim: true
            },
            who:{
                type: String,
                trim: true
            },
            comment: {
                type: String,
                trim: true
            }
       },
    MVP_development:  {
            type: Object,
            task:{
                type: String,
                trim: true
            },
            status: {
                type: String,
                trim: true
            },
            who:{
                type: String,
                trim: true
            },
            comment: {
                type: String,
                trim: true
            }
       },
    quality_assurance:  {
            type: Object,
            task:{
                type: String,
                trim: true
            },
            status: {
                type: String,
                trim: true
            },
            who:{
                type: String,
                trim: true
            },
            comment: {
                type: String,
                trim: true
            }
      },
    launch_and_further_iteration:  {
            type: Object,
            task:{
                type: String,
                trim: true
            },
            status: {
                type: String,
                trim: true
            },
            who:{
                type: String,
                trim: true
            },
            comment: {
                type: String,
                trim: true
            }
       }
  
})


const ServiceModel = mongoose.model('serviceModel', serviceSchema);

module.exports = ServiceModel;