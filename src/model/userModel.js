const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs'); 

var userSchema = new mongoose.Schema({
    name : {
        type : String,
        required: [true, 'Please Enter your name!']
    },
    email : {
        type: String,
        required: [true, 'Please Provide your email'],
        unique: true,
        lowerCase: true,
        validate: [validator.isEmail, 'Please enter a valid email']
    },
    password: {
        type: String,
        required: [true, 'Please enter a password'],
        minLength: 8,
        select: false
    }
   
})

//encrypting our password before it saved to database
userSchema.pre('save', async function(next){
    //only run this function if password is actually modified
    if(!this.isModified('password')) return next();

    //hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12);

    //Deleting the confirm password field
    this.passwordConfirm = undefined;
    next();
});

userSchema.methods.correctPassword = async function(candidatePassword, userPassword){
    return await bcrypt.compare(candidatePassword, userPassword);
}

const Userdb = mongoose.model('userdb', userSchema);

module.exports = Userdb;