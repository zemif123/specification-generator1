const express = require('express');
const router = express.Router()

const websiteController = require('../controller/serviceController');
const authController = require('../controller/authController');


// API
router
      .route('/')
      .post(authController.protect, websiteController.create)
      .get(authController.protect, websiteController.getAllWebsites);

router
      .route('/:id')      
      .get( websiteController.getWebsite)
      .put( websiteController.update)
      .delete( websiteController.delete);


module.exports = router