const express = require('express');
const app = express()




//route for user creation and user authentication
app.use('/api/users', require('./userRouter'))



//route for authentication
app.use('/api/auth', require('./authRouter'))


// route for website Development
app.use('/api/service', require('./serviceRouter'));




module.exports = app