const {promisify} = require('util');
const jwt = require('jsonwebtoken');
const Userdb = require('../model/userModel');
const User =  require('../model/userModel');

exports.signup = async(req, res)=>{
  

      // validate request
      if(!req.body){
        res.status(400).send({ message : "Content can not be empty!"});
        return;
    }

    const newUser = new User({
        name : req.body.name,
        email : req.body.email,
        password: req.body.password,
        passwordConfirm: req.body.passwordConfirm
   
    });

    //creating token
    const token = jwt.sign({id: newUser._id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });

    try {
        const newUser2 = await newUser.save();
        res.status(200).json({
            status: 'Success',
            token,
            data:{
                user: newUser2
            }
        });
    } catch (error) {
        res.send(error);
        
    }
}

exports.login = async (req, res, next)=>{
    const {email, password} = req.body;
    // check if email and password exist
    if(!email || !password){
        res.status(400).send({ message : "please enter email and password"});
        return next();
    }
    
    const user = await Userdb.findOne({email}).select('+password');
    if(!user || !(await user.correctPassword(password, user.password))){
        res.send('Incorrect email or password');
        return next()
    }  
    const token = jwt.sign({id: user._id}, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN
    });
    //if everything okay, send token to client
    try {
        
        res.status(200).json({
            status: 'Success',
            token
        })
    } catch (error) {
        console.log(error);
    }
 
}

exports.logout = (req, res)=>{
    res.status(200).json({
        status: 'Success'
    });
}

exports.protect = async (req, res, next)=>{

         //1 check if the token existed
         let token;
         if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
             token = req.headers.authorization.split(' ')[1];
         }
         console.log(token);
         if(!token){
            res.status(400).send({ message : "You are not logged!"});
            return next();
         }
        // verification token 
         const decoded = await jwt.verify(token, process.env.JWT_SECRET);
         
        try {
            next();
        } catch (error) {
            
            return res.status(400).send({ message : "You are not logged!"});
            
        }
}