const ServiceModel = require('../model/serviceModel');
var fs = require('fs');
var pdf = require('html-pdf');
var options = { format: 'A4' };

exports.create = (req, res)=>{
    // validate request
    if(!req.body){
        res.status(400).send({ message : "Content can not be empty!"});
        return;
    }

    // new user
    const website = new ServiceModel({
        title : req.body.title,
        prepared_for : req.body.prepared_for,
        date: req.body.date,
        registered_feature_by_client : req.body.registered_feature_by_client,
        programming_language : req.body.programming_language,
        frameWork : req.body.frameWork,
        platform_and_database: req.body.platform_and_database,
        discovery_MVP_planning: req.body.discovery_MVP_planning,
        MVP_analysis_design_plan: req.body.MVP_analysis_design_plan,
        MVP_development: req.body.MVP_development,
        quality_assurance: req.body.quality_assurance,
        launch_and_further_iteration: req.body.launch_and_further_iteration


    })

    website.save();
    res.render('index',
                {  
                    title : website.title,
                    prepared_for : website.prepared_for,
                    date: website.date,
                    registered_feature_by_client : website.registered_feature_by_client,
                    programming_language : website.programming_language,
                    frameWork : website.frameWork,
                    platform_and_database: website.platform_and_database,
                    discovery_MVP_planning: website.discovery_MVP_planning,
                    MVP_analysis_design_plan: website.MVP_analysis_design_plan,
                    MVP_development: website.MVP_development,
                    quality_assurance: website.quality_assurance,
                    launch_and_further_iteration: website.launch_and_further_iteration
                }
            ,function(err,html){
                pdf.create(html, options).toFile('./public/uploads/Specification.pdf', function(err, result) {
                    if (err){
                        return console.log(err);
                    }
                    else{
                    console.log(res);
                    var dataFile = fs.readFileSync('./public/uploads/Specification.pdf');
                    res.header('content-type','application/pdf');
                    res.send(dataFile);
                    }
                });
       })


}


exports.getAllWebsites = async(req, res)=>{
    const webs = await ServiceModel.find();
    try {
        res.status(200).json({
            status: 'Success',
            results: webs.length,
            data: {
                webs
            }
        });
    } catch (error) {
        res.send(error);
    }
}


exports.update = async(req, res)=>{
    
}

exports.getWebsite = async(req, res)=>{
    
}




exports.delete = async(req, res)=>{
    
}
