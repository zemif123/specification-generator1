const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const expressLayout = require('express-ejs-layouts')
const bodyParser = require('body-parser')
const path = require('path');
const cors = require('cors')
const connectDB = require('./src/database/connection');
const app = express();

dotenv.config( { path : 'config.env'} )
const PORT = process.env.PORT || 8080


// log requests
app.use(morgan('tiny'));

// mongodb connection
connectDB();
app.use(expressLayout);
// parse request to body-parser
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors());
app.use(express.json());

// set view engine
app.set("view engine", "ejs")
//app.set("/views",  express.static(path.resolve(__dirname, "views")))

// load assets
app.use(express.static("public"))
app.use('/css', express.static(path.resolve(__dirname, "public/css")))
app.use('/img', express.static(path.resolve(__dirname, "public/img")))
app.use('/js', express.static(path.resolve(__dirname, "public/js")))




// load routers
app.use('/', require('./src/routes/router'))

app.listen(PORT, ()=> { console.log(`Server is running on http://localhost:${PORT}`)});


